<?php

class Vt_Categories_Model_System_Config_Source_LinkTargets
{
	public function toOptionArray()
	{
		return array(
			array('value'	=>		'_self',		'label'=>Mage::helper('categories')->__('Same Window')),
        	array('value'	=>		'_blank',		'label'=>Mage::helper('categories')->__('New Window')),
			array('value'	=>		'_windowopen',	'label'=>Mage::helper('categories')->__('Popup Window'))
		);
	}
}
