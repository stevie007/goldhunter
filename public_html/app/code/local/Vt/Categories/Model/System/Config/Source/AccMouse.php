<?php

class Vt_Categories_Model_System_Config_Source_AccMouse
{
	public function toOptionArray()
	{
		return array(
			array('value'	=> 	'click', 		    'label' => Mage::helper('categories')->__('Click')),
			array('value'	=> 	'mouseenter',		'label' => Mage::helper('categories')->__('Hover')),
		);
	}
}
