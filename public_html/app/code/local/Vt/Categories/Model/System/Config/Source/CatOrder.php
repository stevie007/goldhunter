<?php

class Vt_Categories_Model_System_Config_Source_CatOrder
{
	public function toOptionArray()
	{
		return array(
			array('value'	=>  'name', 		'label' => Mage::helper('categories')->__('Name')),
			array('value'	=> 	'position',		'label' => Mage::helper('categories')->__('Position')),
			array('value'	=> 	'random',		'label' => Mage::helper('categories')->__('Random')),
		);
	}
}
