<?php

class Vt_Categories_Model_System_Config_Source_TrueFalse
{
	public function toOptionArray()
	{
		return array(
			array('value'	=> 	true, 		'label' => Mage::helper('categories')->__('True')),
			array('value'	=> 	false,		'label' => Mage::helper('categories')->__('False')),
		);
	}
}
