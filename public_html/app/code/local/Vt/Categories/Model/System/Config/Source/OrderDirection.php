<?php

class Vt_Categories_Model_System_Config_Source_OrderDirection
{
	public function toOptionArray()
	{
		return array(
			array('value'	=>	'ASC',		'label' => Mage::helper('categories')->__('Asc')),
			array('value'	=>	'DESC',		'label' => Mage::helper('categories')->__('Desc'))
		);
	}
}
