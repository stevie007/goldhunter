<?php

class Vt_Categories_Block_List extends Mage_Catalog_Block_Product_Abstract
{
	protected $_config = null;
	protected $_storeId = null;

	public function __construct($attr){
		parent::__construct($attr);
		$this->_config = $this->_getCfg($attr);
		if(!$this->_getConfig('active',1)) return;
		$this->_storeId    = Mage::app()->getStore()->getId();
	}

	public function _getCfg($attr = null)
	{
		// get default config.xml
		$defaults = array();
		$def_cfgs = Mage::getConfig()
			->loadModulesConfiguration('config.xml')
			->getNode('default/categories_cfg')->asArray();
		if(empty($def_cfgs)) return;
		$groups = array();
		foreach($def_cfgs as  $def_key => $def_cfg){
			$groups[] = $def_key;
			foreach($def_cfg as $_def_key =>  $cfg){
				$defaults[$_def_key] = $cfg;
			}
		}

		// get configs after change
		$_configs = (array)Mage::getStoreConfig("categories_cfg");
		if(empty($_configs)) return;
		$cfgs = array();

		foreach($groups as $group){
			$_cfgs = Mage::getStoreConfig('categories_cfg/'. $group.'');
			foreach($_cfgs as $_key =>  $_cfg){
				$cfgs[$_key] = $_cfg;
			}
		}

		// get output config
		$configs = array();
		foreach($defaults as $key => $def){
			if(isset($defaults[$key])){
				$configs[$key] = $cfgs[$key];
			}else{
				unset($cfgs[$key]);
			}
		}
		//var_dump($attr);
		$this->_config =  ($attr != null) ? array_merge($configs,$attr) : $configs;
		return $this->_config;
	}

	public function _getConfig($name = null, $value_def =null){
		if (is_null($this->_config)) $this->_getCfg();
		if (!is_null($name)){
			$value_def = isset($this->_config[$name]) ? $this->_config[$name] : $value_def;
			return $value_def;
		}
		return $this->_config;
	}


	public function _setConfig($name, $value = null){
		if ( is_null( $this->_config ) ) $this->_getCfg();
		if (is_array($name)){
			$this->_config = array_merge($this->_config, $name);
			return;
		}
		if (!empty($name) && isset( $this->_config[$name] ) )
		{
			$this->_config[$name] = $value;
		}
		return true;
	}
	
	protected function _toHtml(){
		if(!$this->_getConfig('active',1)) return;
		$template_file = "vt/categories/default.phtml";
		if(!$this->getTemplate()){
			$this->setTemplate($template_file);
		}else{
			$this->setTemplate($this->getTemplate());
		}
		return parent::_toHtml();
	}	

	/*
	 * Check Categories is Active ?
	 */	
	private function _getCatActive($catids = null,  $orderby = true)
	{
		if(is_null($catids)) 
		{
			$catids = $this->_getConfig('product_category');
		}
		!is_array($catids) && $catids = preg_split('/[\s|,|;]/', $catids, -1, PREG_SPLIT_NO_EMPTY) ;
		if(empty($catids)) return;
			$catidsall = array();
			$categoryIds = array('in'=> $catids);
			$categories = Mage::getModel('catalog/category')
						->getCollection()
						->addAttributeToSelect('*')
						->setStoreId($this->_storeId)
						->addAttributeToFilter('entity_id',  $categoryIds)
						->addIsActiveFilter();

			if ( $orderby )
			{
				$attribute = $this->_getConfig('category_order_by','position');// name | position | entry_id | random
				$dir = $this->_getConfig('category_order_dir','ASC');
				switch($attribute){
					case 'name':
					case 'position':
					case 'entry_id':
						//$categories->addAttributeToSort( $attribute , $dir );
						break;
					case 'random':
						$categories->getSelect()->order(new Zend_Db_Expr('RAND()'));
						break;
					default:	
				}
			}

			$_catids = array();
			if ( empty($categories) )  return;
			foreach($categories as $category)
			{
				$_catids[] = $category->getId();
			}
			
		return $_catids;	
	}

	private function _childCategory($catids, $allcat = true, $limitCat = 0){
		!is_array($catids) && settype($catids, 'array');
		$additional_catids = array();		
		if(!empty($catids)){
			
			foreach($catids as $catid)
			{
				$category_model = Mage::getModel('catalog/category');
				$_category = $category_model->load($catid); 
				$levelCat = $_category->getLevel();				
				 $subcategories = $category_model->getCollection()
				->addAttributeToSelect('*')
				->addFieldToFilter('parent_id', $catid)
				->addIsActiveFilter()
				->addAttributeToSort('position', 'ASC')
				->setPageSize($limitCat)->load();
				foreach($subcategories  as  $each_subcat){
						$additional_catids[] = $each_subcat->getId();	
				}
			}
			
			$catids = $allcat ? array_unique( array_merge( $catids, $additional_catids ) ) :  array_unique( $additional_catids )  ;
		}		
		return $catids;
	}
	
	public function _getCatinfor($catids, $orderby = null){
		!is_array($catids) && settype($catids, 'array');
		$list = array();
		if(!empty($catids))
		{
			$helper = Mage::helper('categories/data');
			foreach($catids as $catid){
				$category_model = Mage::getModel('catalog/category');
				$category = $category_model->load($catid); 				
				$category->title = $category->getName();
				$category->id = $category->entity_id;
				$category->link = $category->getUrl();
				//$category->_description = $helper->_cleanText($category->getDescription());
				//$category->_description =  $helper->_trimEncode($category->description != '') ? $helper->truncate($category->_description,$this->_getConfig('category_description_maxlength',5)) : '';
				//$_image = $helper->getCatImage($category,$this->_getConfig());
				if ($category->getThumbnail() != '') {		
					$category->_image = Mage::getBaseUrl('media').'catalog/category/'.$category->getThumbnail();
				}else{
					$category->_image = '';
				}		
				$category->number_article = $category->getProductCount();				
				$list[$category->id] = (object)$category->getData();
			}
		}
		return $list;
	}
	public function _getList () {
		$catids = $this->_getConfig('product_category');
		if($catids == null) return;
		$_catids = $this->_getCatActive($catids);
		if(empty($_catids))  return;
		$_list = $this->_getCatinfor($_catids, true);
		$list = array();
		$helper = Mage::helper('categories/data');
		foreach ( $_list as $item ) {
			//$item->cat_description = $helper->_cleanText($item->_description);
			$item->child_cat = self::_getChildrenCat($item->id);
			$list[] = $item;
		}

		return $list;
	}

	public function _getChildrenCat ( $catid ) {
		$items = array();
		$limitCat = $this->_getConfig('count_cats',5);
		$_list = self::_childCategory($catid, false, $limitCat);		
		$list = self::_getCatinfor($_list, true);		
		return $list;
	}	
}	