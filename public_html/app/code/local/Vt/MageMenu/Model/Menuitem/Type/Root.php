<?php
/**
 * Vt_MageMenu_Model_Menuitem_Type_Root
 *
 * @package Vt_MageMenu

 * @version 1.5.0
 *
 */
class Vt_MageMenu_Model_Menuitem_Type_Root extends Vt_MageMenu_Model_Menuitem_Type_Abstract
{
	protected $_identifier = 'root';
	protected $_model = null;
	
	public function isValid()
	{
	    return true;
	}
}