<?php
/**
 * Vt_MageMenu_Model_Menuitem_Type_Internal
 *
 * @package Vt_MageMenu

 * @version 1.5.0
 *
 */
class Vt_MageMenu_Model_Menuitem_Type_Internal extends Vt_MageMenu_Model_Menuitem_Type_Abstract
{
	protected $_identifier = 'internal';
	protected $_model = null;
	
	public function getUrl($addBaseUrl = false)
	{
		return $addBaseUrl && $this->getData('link_to_internal') 
					? $this->_addBaseUrl($this->getData('link_to_internal')) 
					: $this->getData('link_to_internal');
	}
	
	public function isActive()
	{
		return $this->getData('link_to_internal');
	}
	
}