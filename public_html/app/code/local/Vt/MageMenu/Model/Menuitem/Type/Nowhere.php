<?php
/**
 * Vt_MageMenu_Model_Menuitem_Type_Nowhere
 *
 * @package Vt_MageMenu

 * @version 1.5.0
 *
 */
class Vt_MageMenu_Model_Menuitem_Type_Nowhere extends Vt_MageMenu_Model_Menuitem_Type_Abstract
{
	protected $_identifier = 'nowhere';
	protected $_model = null;
	
	public function isActive()
	{
		return true;
	}
	
	public function getUrl($addBaseUrl = false)
	{
		return '#';
	}
	
}