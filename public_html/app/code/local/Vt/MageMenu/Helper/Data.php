<?php
/**
 * Vt_MageMenu_Helper_Data
 *
 * @package Vt_MageMenu

 * @version 1.5.0
 *
 */
class Vt_MageMenu_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function isActivated()
	{
		//return Vt_ObBase_Helper_Data::isActivated('Vt_MageMenu', Mage::getStoreConfig('magemenu/general/key'),'magemenu/general/enabled');
		$check = Mage::getStoreConfig('magemenu/general/enabled');
		return $check;
	}
	
	
    public function getAttributeHiddenFields()
    {
        if (Mage::registry('attribute_type_hidden_fields')) {
            return Mage::registry('attribute_type_hidden_fields');
        } else {
            return array();
        }
    }

    public function getAttributeDisabledTypes()
    {
        if (Mage::registry('attribute_type_disabled_types')) {
            return Mage::registry('attribute_type_disabled_types');
        } else {
            return array();
        }
    }
}