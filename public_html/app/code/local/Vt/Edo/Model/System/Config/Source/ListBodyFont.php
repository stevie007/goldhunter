<?php
/*------------------------------------------------------------------------
 # VT Edo - Version 1.0
 # Copyright (c) 2014 The VnThemePro Company. All Rights Reserved.
 # @license - Copyrighted Commercial Software
 # Author: VnThemePro Company
 # Websites: http://www.vnthemepro.com
-------------------------------------------------------------------------*/

class Vt_Edo_Model_System_Config_Source_ListBodyFont
{
	public function toOptionArray()
	{	
		return array(
			array('value'=>'Arial', 'label'=>Mage::helper('edo')->__('Arial')),
			array('value'=>'Arial Black', 'label'=>Mage::helper('edo')->__('Arial-black')),
			array('value'=>'Courier New', 'label'=>Mage::helper('edo')->__('Courier New')),
			array('value'=>'Georgia', 'label'=>Mage::helper('edo')->__('Georgia')),
			array('value'=>'Impact', 'label'=>Mage::helper('edo')->__('Impact')),
			array('value'=>'Lucida Console', 'label'=>Mage::helper('edo')->__('Lucida-console')),
			array('value'=>'Lucida Grande', 'label'=>Mage::helper('edo')->__('Lucida-grande')),
			array('value'=>'Palatino', 'label'=>Mage::helper('edo')->__('Palatino')),
			array('value'=>'Tahoma', 'label'=>Mage::helper('edo')->__('Tahoma')),
			array('value'=>'Times New Roman', 'label'=>Mage::helper('edo')->__('Times New Roman')),	
			array('value'=>'Trebuchet', 'label'=>Mage::helper('edo')->__('Trebuchet')),	
			array('value'=>'Verdana', 'label'=>Mage::helper('edo')->__('Verdana'))		
		);
	}
}
