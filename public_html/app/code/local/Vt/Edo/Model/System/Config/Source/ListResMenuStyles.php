<?php
/*------------------------------------------------------------------------
 # VT Edo - Version 1.0
 # Copyright (c) 2014 The VnThemePro Company. All Rights Reserved.
 # @license - Copyrighted Commercial Software
 # Author: VnThemePro Company
 # Websites: http://www.vnthemepro.com
-------------------------------------------------------------------------*/

class Vt_Edo_Model_System_Config_Source_ListResMenuStyles
{
	public function toOptionArray()
	{	
		return array(
			//array('value'=>'1', 'label'=>Mage::helper('edo')->__('Selectbox')),
			//array('value'=>'2', 'label'=>Mage::helper('edo')->__('Collapse')),
			array('value'=>'3', 'label'=>Mage::helper('edo')->__('Sidebar'))
		);
	}
}
