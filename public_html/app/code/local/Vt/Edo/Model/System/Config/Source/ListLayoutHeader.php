<?php
/*------------------------------------------------------------------------
 # VT Megashop - Version 1.0
 # Copyright (c) 2014 The VnThemePro Company. All Rights Reserved.
 # @license - Copyrighted Commercial Software
 # Author: VnThemePro Company
 # Websites: http://www.vnthemepro.com
-------------------------------------------------------------------------*/

class Vt_Edo_Model_System_Config_Source_ListLayoutHeader
{
	public function toOptionArray()
	{	
		return array(
			array('value'=>'header01', 'label'=>Mage::helper('edo')->__('Header 01')),			
			array('value'=>'header02', 'label'=>Mage::helper('edo')->__('Header 02')),	
			array('value'=>'header03', 'label'=>Mage::helper('edo')->__('Header 03')),	
			array('value'=>'header04', 'label'=>Mage::helper('edo')->__('Header 04')),			
		);
	}
}
