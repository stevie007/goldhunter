<?php
/*------------------------------------------------------------------------
 # VT Edo - Version 1.0
 # Copyright (c) 2014 The VnThemePro Company. All Rights Reserved.
 # @license - Copyrighted Commercial Software
 # Author: VnThemePro Company
 # Websites: http://www.vnthemepro.com
-------------------------------------------------------------------------*/

class Vt_Edo_Model_System_Config_Source_ListGoogleFont
{
	public function toOptionArray()
	{	
		return array(
			array('value'=>'', 'label'=>Mage::helper('edo')->__('No select')),
			array('value'=>'Roboto', 'label'=>Mage::helper('edo')->__('Roboto')),
			array('value'=>'Anton', 'label'=>Mage::helper('edo')->__('Anton')),
			array('value'=>'Questrial', 'label'=>Mage::helper('edo')->__('Questrial')),
			array('value'=>'Kameron', 'label'=>Mage::helper('edo')->__('Kameron')),
			array('value'=>'Oswald', 'label'=>Mage::helper('edo')->__('Oswald')),
			array('value'=>'Open Sans', 'label'=>Mage::helper('edo')->__('Open Sans')),
			array('value'=>'BenchNine', 'label'=>Mage::helper('edo')->__('BenchNine')),
			array('value'=>'Droid Sans', 'label'=>Mage::helper('edo')->__('Droid Sans')),
			array('value'=>'Droid Serif', 'label'=>Mage::helper('edo')->__('Droid Serif')),
			array('value'=>'PT Sans', 'label'=>Mage::helper('edo')->__('PT Sans')),
			array('value'=>'Vollkorn', 'label'=>Mage::helper('edo')->__('Vollkorn')),
			array('value'=>'Ubuntu', 'label'=>Mage::helper('edo')->__('Ubuntu')),
			array('value'=>'Neucha', 'label'=>Mage::helper('edo')->__('Neucha')),
			array('value'=>'Cuprum', 'label'=>Mage::helper('edo')->__('Cuprum'))	
		);
	}
}
