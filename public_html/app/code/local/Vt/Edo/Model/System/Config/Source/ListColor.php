<?php
/*------------------------------------------------------------------------
 # VT Edo - Version 1.0
 # Copyright (c) 2014 The VnThemePro Company. All Rights Reserved.
 # @license - Copyrighted Commercial Software
 # Author: VnThemePro Company
 # Websites: http://www.vnthemepro.com
-------------------------------------------------------------------------*/

class Vt_Edo_Model_System_Config_Source_ListColor
{
	public function toOptionArray()
	{	
		return array(
		array('value'=>'default', 'label'=>Mage::helper('edo')->__('Default')),
		array('value'=>'fa4949', 'label'=>Mage::helper('edo')->__('Color #fa4949')),
		array('value'=>'7bc235', 'label'=>Mage::helper('edo')->__('Color #7bc235')),
		array('value'=>'5dc7cb', 'label'=>Mage::helper('edo')->__('Color #5dc7cb')),	
		array('value'=>'fe983d', 'label'=>Mage::helper('edo')->__('Color #fe983d')),	
		);
	}
}
