<?php
class Vt_Itemslide_Block_List extends Mage_Catalog_Block_Product_Abstract
{
	protected $_config = null;
	public function __construct($attributes = array()){
		parent::__construct($attributes);
		
		if(!Mage::getStoreConfig('itemslide_cfg/general/active')) return;
		$selfData = $this->getData();		
		// handler configuration for module config
		$configuration = $this->_getConfiguration();
		if ( count($configuration) ){
			foreach ($configuration as $field => $value) {
				$selfData[$field] = $value;
			}
		}		
		// handler attributes for {{block ...}} in cms page
		if ( count($attributes) ){
			foreach ($attributes as $field => $value) {
				$selfData[$field] = $value;
			}
		}		
		// re-save data
		$this->setData($selfData);
	}
	public function setConfig($key = null, $value = null){
		if ( is_array($key) ){
			foreach ($key as $k => $v){
				$this->setData($k, $v);
			}
		} else if ( !is_null($key) ) {
			$this->setData($key, $value);
		}
	}
	protected function _getConfiguration($path = 'itemslide_cfg'){
		$configuration = Mage::getStoreConfig($path);
		$conf_merged = array();
		foreach( $configuration as $group ){
			foreach($group as $field => $value){
				if (array_key_exists($field, $conf_merged)){
					// no override
				} else {
					$conf_merged[$field] = $value;
				}
			}
		}
		return $conf_merged;
	}	
	
	protected function _toHtml(){
		$template_file = "vt/itemslide/default.phtml";
		if(! $this->getTemplate()){
			$this->setTemplate($template_file);
		}
		return parent::_toHtml();
	}	
}
