<?php
/*------------------------------------------------------------------------
 # Vt Image Slider - Version 1.0.0
 # Copyright (c) 2014 Vt Company. All Rights Reserved.
 # @license - Copyrighted Commercial Software
 # Author: Vt Company
 # Websites: http://www.7uptheme.com
-------------------------------------------------------------------------*/

class Vt_Itemslide_Model_System_Config_Source_LinkTargets
{
	public function toOptionArray()
	{
		return array(
			array('value'	=>		'_self',		'label'=>Mage::helper('itemslide')->__('Same Window')),
        	array('value'	=>		'_blank',		'label'=>Mage::helper('itemslide')->__('New Window')),
			array('value'	=>		'_windowopen',	'label'=>Mage::helper('itemslide')->__('Popup Window'))
		);
	}
}
