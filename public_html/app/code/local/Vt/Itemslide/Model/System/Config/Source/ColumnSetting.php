<?php
/*------------------------------------------------------------------------
 # Vt Image Slider - Version 1.0.0
 # Copyright (c) 2014 Vt Company. All Rights Reserved.
 # @license - Copyrighted Commercial Software
 # Author: Vt Company
 # Websites: http://www.7uptheme.com
-------------------------------------------------------------------------*/

class Vt_Itemslide_Model_System_Config_Source_ColumnSetting
{
	public function toOptionArray()
	{
		return array(
			array('value' => 1,			'label' => Mage::helper('itemslide')->__('1')),
			array('value' => 2, 		'label' => Mage::helper('itemslide')->__('2')),
			array('value' => 3,			'label' => Mage::helper('itemslide')->__('3')),
			array('value' => 4, 		'label' => Mage::helper('itemslide')->__('4')),
			array('value' => 5,			'label' => Mage::helper('itemslide')->__('5')),
			array('value' => 6, 		'label' => Mage::helper('itemslide')->__('6'))
			
		);
	}
}
