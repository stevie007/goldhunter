<?php
/*------------------------------------------------------------------------
 # Vt Image Slider - Version 1.0.0
 # Copyright (c) 2014 Vt Company. All Rights Reserved.
 # @license - Copyrighted Commercial Software
 # Author: Vt Company
 # Websites: http://www.7uptheme.com
-------------------------------------------------------------------------*/

class Vt_Itemslide_IndexController extends Mage_Core_Controller_Front_Action
{
    public function IndexAction() 
	{		
		$this->loadLayout();
		$this->renderLayout();
    }
	
}